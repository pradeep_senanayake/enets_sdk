package com.nets.enets.view;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.nets.enets.R;
import com.nets.enets.listener.RecyclerTouchListener;
import com.nets.enets.model.PaymentMethod;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Senanayake on 3/13/2017.
 */

public final class PaymentMethodsListProvider extends FrameLayout {

    final         String                     TAG      = getClass().getSimpleName();
    public static PaymentMethodsListProvider provider = null;
    public        BottomSheetDialog          dialog   = null;
    PaymentMethodsAdapter pay_Adapter = null;
    RecyclerView payList;
    Context      context;

    private PaymentMethodsListProvider(@NonNull Context context) {
        super(context);
    }

    public static PaymentMethodsListProvider getSharedInstance(Context context) {
        if (provider == null) {
            provider = new PaymentMethodsListProvider(context);
        }
        return provider;
    }

    public BottomSheetDialog getPaymentMethodsListView(Activity activity) {
        if (activity != null) {
            //TODO is the activity is null, show error dialog....
            //TODO make this bottom sheet either extendable or scrollable
            context = activity.getApplicationContext();
        }

        if (dialog != null) {
            dialog = null;
        }
        View bottomsheet = activity.getLayoutInflater().inflate(R.layout.modal_bottomsheet, null);
        dialog = new BottomSheetDialog(activity);
        dialog.setContentView(bottomsheet);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        initPaymentList(bottomsheet, temp_Init());

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                setNull();
            }
        });
        //  setBottomSheetOpen(bottomsheet);
        return dialog;
    }

    private void setNull() {
        if (dialog != null) {
            dialog = null;
        }
    }

    private void setBottomSheetOpen(View view) {
      /* // final BottomSheetBehavior behavior = BottomSheetBehavior.from(view);
        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                    behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
            }
        });*/

        BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {

            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_HIDDEN) {
//                    Toast.makeText(getContext(), "is selected!", Toast.LENGTH_SHORT).show();

                }
                if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                    // behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
//                    Toast.makeText(getContext(), "is selected!", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
//                Toast.makeText(getContext(), "is selected!", Toast.LENGTH_SHORT).show();
            }
        };

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) (view).getLayoutParams();
        CoordinatorLayout.Behavior behavior = params.getBehavior();

        //Set callback
        if (behavior != null && behavior instanceof BottomSheetBehavior) {
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);
        }


    }

    private ArrayList temp_Init() {
        ArrayList<PaymentMethod> payMethods = new ArrayList<>();
        PaymentMethod method_1 = new PaymentMethod();
        PaymentMethod method_2 = new PaymentMethod();
        PaymentMethod method_3 = new PaymentMethod();

        Bitmap icon_1 = BitmapFactory.decodeResource(context.getResources(), R.drawable.netspay);
        Bitmap icon_2 = BitmapFactory.decodeResource(context.getResources(), R.drawable.logo_paypal);
        Bitmap icon_3 = BitmapFactory.decodeResource(context.getResources(), R.drawable.logo_android_pay);

        method_1.setName("NETS Pay");
        // method_1.setPayImg(icon_1);

        method_2.setName("Pay Pal");
        // method_2.setPayImg(icon_2);

        method_3.setName("Android Pay");
        // method_3.setPayImg(icon_3);
        if (payMethods.size() != 0) {
            payMethods.clear();
        }
        payMethods.add(method_1);
        payMethods.add(method_2);
        payMethods.add(method_3);

        return payMethods;
    }

    private void initPaymentList(View view, final List<PaymentMethod> list) {
        Log.i(TAG, "Payment list size " + list.size());
        payList = (RecyclerView) view.findViewById(R.id.pay_list);
        pay_Adapter = new PaymentMethodsAdapter(list);
        payList.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        payList.setLayoutManager(mLayoutManager);
        payList.addItemDecoration(new PaymentListItemDivider(context));
        payList.setItemAnimator(new DefaultItemAnimator());
        payList.setAdapter(pay_Adapter);

        payList.addOnItemTouchListener(new RecyclerTouchListener(context, payList, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                PaymentMethod pay = list.get(position);
                Log.i(TAG, pay.getName());
                //Toast.makeText(context, pay.getName() + " is selected!", Toast.LENGTH_SHORT).show();
            }
        }));
    }

    final class PaymentMethodsAdapter extends RecyclerView.Adapter<PaymentMethodsAdapter.PaymentsViewHolder> {

        private List<PaymentMethod> payMethodList;

        public class PaymentsViewHolder extends RecyclerView.ViewHolder {

            ImageView pay_image;
            TextView  payment_Name;

            public PaymentsViewHolder(View view) {
                super(view);
                pay_image = (ImageView) view.findViewById(R.id.pay_image);
                payment_Name = (TextView) view.findViewById(R.id.payment_name);
            }
        }

        public PaymentMethodsAdapter(List<PaymentMethod> paymentMethodsList) {
            this.payMethodList = paymentMethodsList;
        }

        @Override
        public PaymentsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.movie_list_row, parent, false);
            return new PaymentsViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(PaymentsViewHolder holder, int position) {
            PaymentMethod pay_Methods = payMethodList.get(position);
            holder.payment_Name.setText(pay_Methods.getName());
            holder.pay_image.setImageBitmap(pay_Methods.getPayImg());
        }

        @Override
        public int getItemCount() {
            return payMethodList.size();
        }
    }

    final class PaymentListItemDivider extends RecyclerView.ItemDecoration {
        private Drawable mDivider;

        public PaymentListItemDivider(Context context) {
            mDivider = ContextCompat.getDrawable(context, R.drawable.line_divider);
        }

        @Override
        public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
            int left = parent.getPaddingLeft();
            int right = parent.getWidth() - parent.getPaddingRight();

            int childCount = parent.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View child = parent.getChildAt(i);

                RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

                int top = child.getBottom() + params.bottomMargin;
                int bottom = top + mDivider.getIntrinsicHeight();

                mDivider.setBounds(left, top, right, bottom);
                mDivider.draw(c);
            }
        }
    }
}

package com.nets.enets.model;

import android.graphics.Bitmap;

/**
 * Created by Senanayake on 3/14/2017.
 */

public final class PaymentMethod {

    private Bitmap payImg;
    private String name;

    public Bitmap getPayImg() {
        return payImg;
    }

    public void setPayImg(Bitmap payImg) {
        this.payImg = payImg;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}

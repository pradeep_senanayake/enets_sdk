package com.nets.enets.view;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nets.enets.R;
import com.nets.enets.listener.RecyclerTouchListener;
import com.nets.enets.model.PaymentMethod;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Senanayake on 3/13/2017.
 */


public final class PaymentMethodsListFragment extends BottomSheetDialogFragment {

    final         String                     TAG               = getClass().getSimpleName();
    public static PaymentMethodsListFragment paymentMethodList = null;
    PaymentMethodsListFragment.PaymentMethodsAdapter pay_Adapter = null;
    RecyclerView payList;
    private Context globalContext;


    public PaymentMethodsListFragment() {
    }

    public static PaymentMethodsListFragment getSharedInstance() {
        if (paymentMethodList == null) {
            paymentMethodList = new PaymentMethodsListFragment();
        }
        return paymentMethodList;
    }


    //Bottom Sheet Callback
    public BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {

        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss();
            }
            if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                // behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                // dismiss();
            }

        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {
        }
    };

    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        //Get the content View
        View contentView = View.inflate(getContext(), R.layout.modal_bottomsheet, null);

        if (contentView.getParent() != null) {
            ViewGroup parent = (ViewGroup) contentView.getParent();

            if (parent != null) {
                parent.removeView(contentView);
            }
        }

        dialog.setContentView(contentView);

        //Set the coordinator layout behavior
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) contentView.getParent()).getLayoutParams();
        CoordinatorLayout.Behavior behavior = params.getBehavior();

        //Set callback
        if (behavior != null && behavior instanceof BottomSheetBehavior) {
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);
        }
        globalContext = getContext();
        initPaymentList(contentView, temp_Init());
    }


    private ArrayList temp_Init() {
        ArrayList<PaymentMethod> payMethods = new ArrayList<>();
        PaymentMethod method_1 = new PaymentMethod();
        PaymentMethod method_2 = new PaymentMethod();
        PaymentMethod method_3 = new PaymentMethod();
        PaymentMethod method_4 = new PaymentMethod();

        Bitmap icon_1 = BitmapFactory.decodeResource(getGlobalContext().getResources(), R.drawable.netspay);
        Bitmap icon_2 = BitmapFactory.decodeResource(getGlobalContext().getResources(), R.drawable.logo_paypal);
        Bitmap icon_3 = BitmapFactory.decodeResource(getGlobalContext().getResources(), R.drawable.logo_android_pay);
        Bitmap icon_4 = BitmapFactory.decodeResource(getGlobalContext().getResources(), R.drawable.debit_credit);

        method_1.setName("NETS Pay");
        method_1.setPayImg(icon_1);

        method_2.setName("Pay Pal");
        method_2.setPayImg(icon_2);

        method_3.setName("Android Pay");
        method_3.setPayImg(icon_3);

        method_4.setName("Debit/Credit");
        method_4.setPayImg(icon_4);

        if (payMethods.size() != 0) {
            payMethods.clear();
        }
        payMethods.add(method_1);
        payMethods.add(method_2);
        payMethods.add(method_3);
        payMethods.add(method_4);

        return payMethods;
    }

    private void initPaymentList(View view, final List<PaymentMethod> list) {
        Log.i(TAG, "Payment list size " + list.size());
        payList = (RecyclerView) view.findViewById(R.id.pay_list);
        pay_Adapter = new PaymentMethodsListFragment.PaymentMethodsAdapter(list);
        payList.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getGlobalContext());
        payList.setLayoutManager(mLayoutManager);
        payList.addItemDecoration(new PaymentMethodsListFragment.PaymentListItemDivider(getGlobalContext()));
        payList.setItemAnimator(new DefaultItemAnimator());
        payList.setAdapter(pay_Adapter);

        payList.addOnItemTouchListener(new RecyclerTouchListener(getGlobalContext(), payList, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                PaymentMethod pay = list.get(position);
                Log.i(TAG, pay.getName());
                if (pay.getName().equalsIgnoreCase("Debit/Credit")) {
                    Intent credit_debit = new Intent(getGlobalContext(), PaymentCardDetails.class);
                    getGlobalContext().startActivity(credit_debit);
                }
            }
        }));
    }

    public void setGlobalContext(Context globalContext) {
        this.globalContext = globalContext;
    }

    public Context getGlobalContext() {
        return globalContext;
    }

    final class PaymentMethodsAdapter extends RecyclerView.Adapter<PaymentMethodsAdapter.PaymentsViewHolder> {

        private List<PaymentMethod> payMethodList;

        public class PaymentsViewHolder extends RecyclerView.ViewHolder {

            ImageView pay_image;
            TextView  payment_Name;

            public PaymentsViewHolder(View view) {
                super(view);
                pay_image = (ImageView) view.findViewById(R.id.pay_image);
                payment_Name = (TextView) view.findViewById(R.id.payment_name);
            }
        }

        public PaymentMethodsAdapter(List<PaymentMethod> paymentMethodsList) {
            this.payMethodList = paymentMethodsList;
        }

        @Override
        public PaymentsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.movie_list_row, parent, false);
            return new PaymentsViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(PaymentsViewHolder holder, int position) {
            PaymentMethod pay_Methods = payMethodList.get(position);
            holder.payment_Name.setText(pay_Methods.getName());
            holder.pay_image.setImageBitmap(pay_Methods.getPayImg());
        }

        @Override
        public int getItemCount() {
            return payMethodList.size();
        }
    }

    final class PaymentListItemDivider extends RecyclerView.ItemDecoration {
        private Drawable mDivider;

        public PaymentListItemDivider(Context context) {
            mDivider = ContextCompat.getDrawable(context, R.drawable.line_divider);
        }

        @Override
        public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
            int left = parent.getPaddingLeft();
            int right = parent.getWidth() - parent.getPaddingRight();

            int childCount = parent.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View child = parent.getChildAt(i);

                RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

                int top = child.getBottom() + params.bottomMargin;
                int bottom = top + mDivider.getIntrinsicHeight();

                mDivider.setBounds(left, top, right, bottom);
                mDivider.draw(c);
            }
        }
    }

}

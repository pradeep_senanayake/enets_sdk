package com.nets.enets.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.nets.enets.R;


public final class PaymentCardDetails extends ENETSBasicActivity implements View.OnClickListener {

    EditText cardNumber_txt;
    EditText date_txt;
    EditText cvv_txt;
    Button   next;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_card_details);
        initComponents();
        setDateTxtListener();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        next.setOnClickListener(this);
    }

    private void initComponents() {

        cardNumber_txt = (EditText) findViewById(R.id.cardnumber_txt);
        date_txt = (EditText) findViewById(R.id.date_txt);
        cvv_txt = (EditText) findViewById(R.id.cvv_txt);
        next = (Button) findViewById(R.id.next_btn);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void setDateTxtListener() {
        date_txt.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
                if (s.length() == 2) {
                    date_txt.setText(s.append('/'));
                    Editable editObj = date_txt.getText();
                    Selection.setSelection(editObj, 3);
                }
                if (s.length() == 5) {
                    cvv_txt.requestFocus();
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (s.length() == 5) {
                    cvv_txt.requestFocus();
                }
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 5) {
                    cvv_txt.requestFocus();
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        Intent web = new Intent(this, OTPCallBackWebView.class);
        startActivity(web);
    }
}

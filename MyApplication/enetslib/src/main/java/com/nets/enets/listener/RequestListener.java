package com.nets.enets.listener;

/**
 * Created by Senanayake on 3/14/2017.
 */

public interface RequestListener {

    public void onSuccess();

    public void onFailed();
}

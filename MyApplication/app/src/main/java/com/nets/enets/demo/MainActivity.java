package com.nets.enets.demo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.nets.enets.view.PaymentMethodsListFragment;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button show;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        show = (Button) findViewById(R.id.sample_btn);
        show.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        PaymentMethodsListFragment bottomSheetDialogFragment = PaymentMethodsListFragment.getSharedInstance();
        bottomSheetDialogFragment.show(getSupportFragmentManager(), "Bottom Sheet Dialog Fragment");
    }
}
